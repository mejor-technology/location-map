<?php

$countyDistricMapZipcodeAry = array (
  '基隆市' =>
  array (
    '仁愛區' => '200',
    '信義區' => '201',
    '中正區' => '202',
    '中山區' => '203',
    '安樂區' => '204',
    '暖暖區' => '205',
    '七堵區' => '206',
  ),
  '台北市' =>
  array (
    '中正區' => '100',
    '大同區' => '103',
    '中山區' => '104',
    '松山區' => '105',
    '大安區' => '106',
    '萬華區' => '108',
    '信義區' => '110',
    '士林區' => '111',
    '北投區' => '112',
    '內湖區' => '114',
    '南港區' => '115',
    '文山區' => '116',
  ),
  '新北市' =>
  array (
    '萬里區' => '207',
    '金山區' => '208',
    '板橋區' => '220',
    '汐止區' => '221',
    '深坑區' => '222',
    '石碇區' => '223',
    '瑞芳區' => '224',
    '平溪區' => '226',
    '雙溪區' => '227',
    '貢寮區' => '228',
    '新店區' => '231',
    '坪林區' => '232',
    '烏來區' => '233',
    '永和區' => '234',
    '中和區' => '235',
    '土城區' => '236',
    '三峽區' => '237',
    '樹林區' => '238',
    '鶯歌區' => '239',
    '三重區' => '241',
    '新莊區' => '242',
    '泰山區' => '243',
    '林口區' => '244',
    '蘆洲區' => '247',
    '五股區' => '248',
    '八里區' => '249',
    '淡水區' => '251',
    '三芝區' => '252',
    '石門區' => '253',
  ),
  '宜蘭縣' =>
  array (
    '宜蘭市' => '260',
    '頭城鎮' => '261',
    '礁溪鄉' => '262',
    '壯圍鄉' => '263',
    '員山鄉' => '264',
    '羅東鎮' => '265',
    '三星鄉' => '266',
    '大同鄉' => '267',
    '五結鄉' => '268',
    '冬山鄉' => '269',
    '蘇澳鎮' => '270',
    '南澳鄉' => '272',
    '釣魚台列嶼' => '290',
  ),
  '新竹市' =>
  array (
    '東區' => '300',
    '北區' => '300',
    '香山區' => '300',
  ),
  '新竹縣' =>
  array (
    '竹北市' => '302',
    '湖口鄉' => '303',
    '新豐鄉' => '304',
    '新埔鎮' => '305',
    '關西鎮' => '306',
    '芎林鄉' => '307',
    '寶山鄉' => '308',
    '竹東鎮' => '310',
    '五峰鄉' => '311',
    '橫山鄉' => '312',
    '尖石鄉' => '313',
    '北埔鄉' => '314',
    '峨嵋鄉' => '315',
  ),
  '桃園縣' =>
  array (
    '中壢市' => '320',
    '平鎮市' => '324',
    '龍潭鄉' => '325',
    '楊梅鎮' => '326',
    '新屋鄉' => '327',
    '觀音鄉' => '328',
    '桃園市' => '330',
    '龜山鄉' => '333',
    '八德市' => '334',
    '大溪鎮' => '335',
    '復興鄉' => '336',
    '大園鄉' => '337',
    '蘆竹鄉' => '338',
  ),
  '苗栗縣' =>
  array (
    '竹南鎮' => '350',
    '頭份鎮' => '351',
    '三灣鄉' => '352',
    '南庄鄉' => '353',
    '獅潭鄉' => '354',
    '後龍鎮' => '356',
    '通霄鎮' => '357',
    '苑裡鎮' => '358',
    '苗栗市' => '360',
    '造橋鄉' => '361',
    '頭屋鄉' => '362',
    '公館鄉' => '363',
    '大湖鄉' => '364',
    '泰安鄉' => '365',
    '銅鑼鄉' => '366',
    '三義鄉' => '367',
    '西湖鄉' => '368',
    '卓蘭鎮' => '369',
  ),
  '台中市' =>
  array (
    '中區' => '400',
    '東區' => '401',
    '南區' => '402',
    '西區' => '403',
    '北區' => '404',
    '北屯區' => '406',
    '西屯區' => '407',
    '南屯區' => '408',
    '太平區' => '411',
    '大里區' => '412',
    '霧峰區' => '413',
    '烏日區' => '414',
    '豐原區' => '420',
    '后里區' => '421',
    '石岡區' => '422',
    '東勢區' => '423',
    '和平區' => '424',
    '新社區' => '426',
    '潭子區' => '427',
    '大雅區' => '428',
    '神岡區' => '429',
    '大肚區' => '432',
    '沙鹿區' => '433',
    '龍井區' => '434',
    '梧棲區' => '435',
    '清水區' => '436',
    '大甲區' => '437',
    '外埔區' => '438',
    '大安區' => '439',
  ),
  '彰化縣' =>
  array (
    '彰化市' => '500',
    '芬園鄉' => '502',
    '花壇鄉' => '503',
    '秀水鄉' => '504',
    '鹿港鎮' => '505',
    '福興鄉' => '506',
    '線西鄉' => '507',
    '和美鎮' => '508',
    '伸港鄉' => '509',
    '員林鎮' => '510',
    '社頭鄉' => '511',
    '永靖鄉' => '512',
    '埔心鄉' => '513',
    '溪湖鎮' => '514',
    '大村鄉' => '515',
    '埔鹽鄉' => '516',
    '田中鎮' => '520',
    '北斗鎮' => '521',
    '田尾鄉' => '522',
    '埤頭鄉' => '523',
    '溪州鄉' => '524',
    '竹塘鄉' => '525',
    '二林鎮' => '526',
    '大城鄉' => '527',
    '芳苑鄉' => '528',
    '二水鄉' => '530',
  ),
  '南投縣' =>
  array (
    '南投市' => '540',
    '中寮鄉' => '541',
    '草屯鎮' => '542',
    '國姓鄉' => '544',
    '埔里鎮' => '545',
    '仁愛鄉' => '546',
    '名間鄉' => '551',
    '集集鎮' => '552',
    '水里鄉' => '553',
    '魚池鄉' => '555',
    '信義鄉' => '556',
    '竹山鎮' => '557',
    '鹿谷鄉' => '558',
  ),
  '嘉義市' =>
  array (
    '東區' => '600',
    '西區' => '600',
  ),
  '嘉義縣' =>
  array (
    '番路鄉' => '602',
    '梅山鄉' => '603',
    '竹崎鄉' => '604',
    '阿里山' => '605',
    '中埔鄉' => '606',
    '大埔鄉' => '607',
    '水上鄉' => '608',
    '鹿草鄉' => '611',
    '太保市' => '612',
    '朴子市' => '613',
    '東石鄉' => '614',
    '六腳鄉' => '615',
    '新港鄉' => '616',
    '民雄鄉' => '621',
    '大林鎮' => '622',
    '溪口鄉' => '623',
    '義竹鄉' => '624',
    '布袋鎮' => '625',
  ),
  '雲林縣' =>
  array (
    '斗南鎮' => '630',
    '大埤鄉' => '631',
    '虎尾鎮' => '632',
    '土庫鎮' => '633',
    '褒忠鄉' => '634',
    '東勢鄉' => '635',
    '臺西鄉' => '636',
    '崙背鄉' => '637',
    '麥寮鄉' => '638',
    '斗六市' => '640',
    '林內鄉' => '643',
    '古坑鄉' => '646',
    '莿桐鄉' => '647',
    '西螺鎮' => '648',
    '二崙鄉' => '649',
    '北港鎮' => '651',
    '水林鄉' => '652',
    '口湖鄉' => '653',
    '四湖鄉' => '654',
    '元長鄉' => '655',
  ),
  '台南市' =>
  array (
    '中西區' => '700',
    '東區' => '701',
    '南區' => '702',
    '北區' => '704',
    '安平區' => '708',
    '安南區' => '709',
    '永康區' => '710',
    '歸仁區' => '711',
    '新化區' => '712',
    '左鎮區' => '713',
    '玉井區' => '714',
    '楠西區' => '715',
    '南化區' => '716',
    '仁德區' => '717',
    '關廟區' => '718',
    '龍崎區' => '719',
    '官田區' => '720',
    '麻豆區' => '721',
    '佳里區' => '722',
    '西港區' => '723',
    '七股區' => '724',
    '將軍區' => '725',
    '學甲區' => '726',
    '北門區' => '727',
    '新營區' => '730',
    '後壁區' => '731',
    '白河區' => '732',
    '東山區' => '733',
    '六甲區' => '734',
    '下營區' => '735',
    '柳營區' => '736',
    '鹽水區' => '737',
    '善化區' => '741',
    '大內區' => '742',
    '山上區' => '743',
    '新市區' => '744',
    '安定區' => '745',
  ),
  '高雄市' =>
  array (
    '新興區' => '800',
    '前金區' => '801',
    '苓雅區' => '802',
    '鹽埕區' => '803',
    '鼓山區' => '804',
    '旗津區' => '805',
    '前鎮區' => '806',
    '三民區' => '807',
    '楠梓區' => '811',
    '小港區' => '812',
    '左營區' => '813',
    '仁武區' => '814',
    '大社區' => '815',
    '岡山區' => '820',
    '路竹區' => '821',
    '阿蓮區' => '822',
    '田寮鄉' => '823',
    '燕巢區' => '824',
    '橋頭區' => '825',
    '梓官區' => '826',
    '彌陀區' => '827',
    '永安區' => '828',
    '湖內鄉' => '829',
    '鳳山區' => '830',
    '大寮區' => '831',
    '林園區' => '832',
    '鳥松區' => '833',
    '大樹區' => '840',
    '旗山區' => '842',
    '美濃區' => '843',
    '六龜區' => '844',
    '內門區' => '845',
    '杉林區' => '846',
    '甲仙區' => '847',
    '桃源區' => '848',
    '那瑪夏區' => '849',
    '茂林區' => '851',
    '茄萣區' => '852',
  ),
  '屏東縣' =>
  array (
    '屏東市' => '900',
    '三地門' => '901',
    '霧臺鄉' => '902',
    '瑪家鄉' => '903',
    '九如鄉' => '904',
    '里港鄉' => '905',
    '高樹鄉' => '906',
    '鹽埔鄉' => '907',
    '長治鄉' => '908',
    '麟洛鄉' => '909',
    '竹田鄉' => '911',
    '內埔鄉' => '912',
    '萬丹鄉' => '913',
    '潮州鎮' => '920',
    '泰武鄉' => '921',
    '來義鄉' => '922',
    '萬巒鄉' => '923',
    '崁頂鄉' => '924',
    '新埤鄉' => '925',
    '南州鄉' => '926',
    '林邊鄉' => '927',
    '東港鎮' => '928',
    '琉球鄉' => '929',
    '佳冬鄉' => '931',
    '新園鄉' => '932',
    '枋寮鄉' => '940',
    '枋山鄉' => '941',
    '春日鄉' => '942',
    '獅子鄉' => '943',
    '車城鄉' => '944',
    '牡丹鄉' => '945',
    '恆春鎮' => '946',
    '滿州鄉' => '947',
  ),
  '台東縣' =>
  array (
    '臺東市' => '950',
    '綠島鄉' => '951',
    '蘭嶼鄉' => '952',
    '延平鄉' => '953',
    '卑南鄉' => '954',
    '鹿野鄉' => '955',
    '關山鎮' => '956',
    '海端鄉' => '957',
    '池上鄉' => '958',
    '東河鄉' => '959',
    '成功鎮' => '961',
    '長濱鄉' => '962',
    '太麻里鄉' => '963',
    '金峰鄉' => '964',
    '大武鄉' => '965',
    '達仁鄉' => '966',
  ),
  '花蓮縣' =>
  array (
    '花蓮市' => '970',
    '新城鄉' => '971',
    '秀林鄉' => '972',
    '吉安鄉' => '973',
    '壽豐鄉' => '974',
    '鳳林鎮' => '975',
    '光復鄉' => '976',
    '豐濱鄉' => '977',
    '瑞穗鄉' => '978',
    '萬榮鄉' => '979',
    '玉里鎮' => '981',
    '卓溪鄉' => '982',
    '富里鄉' => '983',
  ),
  '金門縣' =>
  array (
    '金沙鎮' => '890',
    '金湖鎮' => '891',
    '金寧鄉' => '892',
    '金城鎮' => '893',
    '烈嶼鄉' => '894',
    '烏坵鄉' => '896',
  ),
  '連江縣' =>
  array (
    '南竿鄉' => '209',
    '北竿鄉' => '210',
    '莒光鄉' => '211',
    '東引鄉' => '212',
  ),
  '澎湖縣' =>
  array (
    '馬公市' => '880',
    '西嶼鄉' => '881',
    '望安鄉' => '882',
    '七美鄉' => '883',
    '白沙鄉' => '884',
    '湖西鄉' => '885',
  ),
  '南海諸島' =>
  array (
    '東沙' => '817',
    '南沙' => '819',
  ),
);

$zipcodeMapCountyAndDistrictAry = array (
  200 =>
  array (
    'county' => '基隆市',
    'district' => '仁愛區',
  ),
  201 =>
  array (
    'county' => '基隆市',
    'district' => '信義區',
  ),
  202 =>
  array (
    'county' => '基隆市',
    'district' => '中正區',
  ),
  203 =>
  array (
    'county' => '基隆市',
    'district' => '中山區',
  ),
  204 =>
  array (
    'county' => '基隆市',
    'district' => '安樂區',
  ),
  205 =>
  array (
    'county' => '基隆市',
    'district' => '暖暖區',
  ),
  206 =>
  array (
    'county' => '基隆市',
    'district' => '七堵區',
  ),
  100 =>
  array (
    'county' => '台北市',
    'district' => '中正區',
  ),
  103 =>
  array (
    'county' => '台北市',
    'district' => '大同區',
  ),
  104 =>
  array (
    'county' => '台北市',
    'district' => '中山區',
  ),
  105 =>
  array (
    'county' => '台北市',
    'district' => '松山區',
  ),
  106 =>
  array (
    'county' => '台北市',
    'district' => '大安區',
  ),
  108 =>
  array (
    'county' => '台北市',
    'district' => '萬華區',
  ),
  110 =>
  array (
    'county' => '台北市',
    'district' => '信義區',
  ),
  111 =>
  array (
    'county' => '台北市',
    'district' => '士林區',
  ),
  112 =>
  array (
    'county' => '台北市',
    'district' => '北投區',
  ),
  114 =>
  array (
    'county' => '台北市',
    'district' => '內湖區',
  ),
  115 =>
  array (
    'county' => '台北市',
    'district' => '南港區',
  ),
  116 =>
  array (
    'county' => '台北市',
    'district' => '文山區',
  ),
  207 =>
  array (
    'county' => '新北市',
    'district' => '萬里區',
  ),
  208 =>
  array (
    'county' => '新北市',
    'district' => '金山區',
  ),
  220 =>
  array (
    'county' => '新北市',
    'district' => '板橋區',
  ),
  221 =>
  array (
    'county' => '新北市',
    'district' => '汐止區',
  ),
  222 =>
  array (
    'county' => '新北市',
    'district' => '深坑區',
  ),
  223 =>
  array (
    'county' => '新北市',
    'district' => '石碇區',
  ),
  224 =>
  array (
    'county' => '新北市',
    'district' => '瑞芳區',
  ),
  226 =>
  array (
    'county' => '新北市',
    'district' => '平溪區',
  ),
  227 =>
  array (
    'county' => '新北市',
    'district' => '雙溪區',
  ),
  228 =>
  array (
    'county' => '新北市',
    'district' => '貢寮區',
  ),
  231 =>
  array (
    'county' => '新北市',
    'district' => '新店區',
  ),
  232 =>
  array (
    'county' => '新北市',
    'district' => '坪林區',
  ),
  233 =>
  array (
    'county' => '新北市',
    'district' => '烏來區',
  ),
  234 =>
  array (
    'county' => '新北市',
    'district' => '永和區',
  ),
  235 =>
  array (
    'county' => '新北市',
    'district' => '中和區',
  ),
  236 =>
  array (
    'county' => '新北市',
    'district' => '土城區',
  ),
  237 =>
  array (
    'county' => '新北市',
    'district' => '三峽區',
  ),
  238 =>
  array (
    'county' => '新北市',
    'district' => '樹林區',
  ),
  239 =>
  array (
    'county' => '新北市',
    'district' => '鶯歌區',
  ),
  241 =>
  array (
    'county' => '新北市',
    'district' => '三重區',
  ),
  242 =>
  array (
    'county' => '新北市',
    'district' => '新莊區',
  ),
  243 =>
  array (
    'county' => '新北市',
    'district' => '泰山區',
  ),
  244 =>
  array (
    'county' => '新北市',
    'district' => '林口區',
  ),
  247 =>
  array (
    'county' => '新北市',
    'district' => '蘆洲區',
  ),
  248 =>
  array (
    'county' => '新北市',
    'district' => '五股區',
  ),
  249 =>
  array (
    'county' => '新北市',
    'district' => '八里區',
  ),
  251 =>
  array (
    'county' => '新北市',
    'district' => '淡水區',
  ),
  252 =>
  array (
    'county' => '新北市',
    'district' => '三芝區',
  ),
  253 =>
  array (
    'county' => '新北市',
    'district' => '石門區',
  ),
  260 =>
  array (
    'county' => '宜蘭縣',
    'district' => '宜蘭市',
  ),
  261 =>
  array (
    'county' => '宜蘭縣',
    'district' => '頭城鎮',
  ),
  262 =>
  array (
    'county' => '宜蘭縣',
    'district' => '礁溪鄉',
  ),
  263 =>
  array (
    'county' => '宜蘭縣',
    'district' => '壯圍鄉',
  ),
  264 =>
  array (
    'county' => '宜蘭縣',
    'district' => '員山鄉',
  ),
  265 =>
  array (
    'county' => '宜蘭縣',
    'district' => '羅東鎮',
  ),
  266 =>
  array (
    'county' => '宜蘭縣',
    'district' => '三星鄉',
  ),
  267 =>
  array (
    'county' => '宜蘭縣',
    'district' => '大同鄉',
  ),
  268 =>
  array (
    'county' => '宜蘭縣',
    'district' => '五結鄉',
  ),
  269 =>
  array (
    'county' => '宜蘭縣',
    'district' => '冬山鄉',
  ),
  270 =>
  array (
    'county' => '宜蘭縣',
    'district' => '蘇澳鎮',
  ),
  272 =>
  array (
    'county' => '宜蘭縣',
    'district' => '南澳鄉',
  ),
  290 =>
  array (
    'county' => '宜蘭縣',
    'district' => '釣魚台列嶼',
  ),
  300 =>
  array (
    'county' => '新竹市',
    'district' => '香山區',
  ),
  302 =>
  array (
    'county' => '新竹縣',
    'district' => '竹北市',
  ),
  303 =>
  array (
    'county' => '新竹縣',
    'district' => '湖口鄉',
  ),
  304 =>
  array (
    'county' => '新竹縣',
    'district' => '新豐鄉',
  ),
  305 =>
  array (
    'county' => '新竹縣',
    'district' => '新埔鎮',
  ),
  306 =>
  array (
    'county' => '新竹縣',
    'district' => '關西鎮',
  ),
  307 =>
  array (
    'county' => '新竹縣',
    'district' => '芎林鄉',
  ),
  308 =>
  array (
    'county' => '新竹縣',
    'district' => '寶山鄉',
  ),
  310 =>
  array (
    'county' => '新竹縣',
    'district' => '竹東鎮',
  ),
  311 =>
  array (
    'county' => '新竹縣',
    'district' => '五峰鄉',
  ),
  312 =>
  array (
    'county' => '新竹縣',
    'district' => '橫山鄉',
  ),
  313 =>
  array (
    'county' => '新竹縣',
    'district' => '尖石鄉',
  ),
  314 =>
  array (
    'county' => '新竹縣',
    'district' => '北埔鄉',
  ),
  315 =>
  array (
    'county' => '新竹縣',
    'district' => '峨嵋鄉',
  ),
  320 =>
  array (
    'county' => '桃園縣',
    'district' => '中壢市',
  ),
  324 =>
  array (
    'county' => '桃園縣',
    'district' => '平鎮市',
  ),
  325 =>
  array (
    'county' => '桃園縣',
    'district' => '龍潭鄉',
  ),
  326 =>
  array (
    'county' => '桃園縣',
    'district' => '楊梅鎮',
  ),
  327 =>
  array (
    'county' => '桃園縣',
    'district' => '新屋鄉',
  ),
  328 =>
  array (
    'county' => '桃園縣',
    'district' => '觀音鄉',
  ),
  330 =>
  array (
    'county' => '桃園縣',
    'district' => '桃園市',
  ),
  333 =>
  array (
    'county' => '桃園縣',
    'district' => '龜山鄉',
  ),
  334 =>
  array (
    'county' => '桃園縣',
    'district' => '八德市',
  ),
  335 =>
  array (
    'county' => '桃園縣',
    'district' => '大溪鎮',
  ),
  336 =>
  array (
    'county' => '桃園縣',
    'district' => '復興鄉',
  ),
  337 =>
  array (
    'county' => '桃園縣',
    'district' => '大園鄉',
  ),
  338 =>
  array (
    'county' => '桃園縣',
    'district' => '蘆竹鄉',
  ),
  350 =>
  array (
    'county' => '苗栗縣',
    'district' => '竹南鎮',
  ),
  351 =>
  array (
    'county' => '苗栗縣',
    'district' => '頭份鎮',
  ),
  352 =>
  array (
    'county' => '苗栗縣',
    'district' => '三灣鄉',
  ),
  353 =>
  array (
    'county' => '苗栗縣',
    'district' => '南庄鄉',
  ),
  354 =>
  array (
    'county' => '苗栗縣',
    'district' => '獅潭鄉',
  ),
  356 =>
  array (
    'county' => '苗栗縣',
    'district' => '後龍鎮',
  ),
  357 =>
  array (
    'county' => '苗栗縣',
    'district' => '通霄鎮',
  ),
  358 =>
  array (
    'county' => '苗栗縣',
    'district' => '苑裡鎮',
  ),
  360 =>
  array (
    'county' => '苗栗縣',
    'district' => '苗栗市',
  ),
  361 =>
  array (
    'county' => '苗栗縣',
    'district' => '造橋鄉',
  ),
  362 =>
  array (
    'county' => '苗栗縣',
    'district' => '頭屋鄉',
  ),
  363 =>
  array (
    'county' => '苗栗縣',
    'district' => '公館鄉',
  ),
  364 =>
  array (
    'county' => '苗栗縣',
    'district' => '大湖鄉',
  ),
  365 =>
  array (
    'county' => '苗栗縣',
    'district' => '泰安鄉',
  ),
  366 =>
  array (
    'county' => '苗栗縣',
    'district' => '銅鑼鄉',
  ),
  367 =>
  array (
    'county' => '苗栗縣',
    'district' => '三義鄉',
  ),
  368 =>
  array (
    'county' => '苗栗縣',
    'district' => '西湖鄉',
  ),
  369 =>
  array (
    'county' => '苗栗縣',
    'district' => '卓蘭鎮',
  ),
  400 =>
  array (
    'county' => '台中市',
    'district' => '中區',
  ),
  401 =>
  array (
    'county' => '台中市',
    'district' => '東區',
  ),
  402 =>
  array (
    'county' => '台中市',
    'district' => '南區',
  ),
  403 =>
  array (
    'county' => '台中市',
    'district' => '西區',
  ),
  404 =>
  array (
    'county' => '台中市',
    'district' => '北區',
  ),
  406 =>
  array (
    'county' => '台中市',
    'district' => '北屯區',
  ),
  407 =>
  array (
    'county' => '台中市',
    'district' => '西屯區',
  ),
  408 =>
  array (
    'county' => '台中市',
    'district' => '南屯區',
  ),
  411 =>
  array (
    'county' => '台中市',
    'district' => '太平區',
  ),
  412 =>
  array (
    'county' => '台中市',
    'district' => '大里區',
  ),
  413 =>
  array (
    'county' => '台中市',
    'district' => '霧峰區',
  ),
  414 =>
  array (
    'county' => '台中市',
    'district' => '烏日區',
  ),
  420 =>
  array (
    'county' => '台中市',
    'district' => '豐原區',
  ),
  421 =>
  array (
    'county' => '台中市',
    'district' => '后里區',
  ),
  422 =>
  array (
    'county' => '台中市',
    'district' => '石岡區',
  ),
  423 =>
  array (
    'county' => '台中市',
    'district' => '東勢區',
  ),
  424 =>
  array (
    'county' => '台中市',
    'district' => '和平區',
  ),
  426 =>
  array (
    'county' => '台中市',
    'district' => '新社區',
  ),
  427 =>
  array (
    'county' => '台中市',
    'district' => '潭子區',
  ),
  428 =>
  array (
    'county' => '台中市',
    'district' => '大雅區',
  ),
  429 =>
  array (
    'county' => '台中市',
    'district' => '神岡區',
  ),
  432 =>
  array (
    'county' => '台中市',
    'district' => '大肚區',
  ),
  433 =>
  array (
    'county' => '台中市',
    'district' => '沙鹿區',
  ),
  434 =>
  array (
    'county' => '台中市',
    'district' => '龍井區',
  ),
  435 =>
  array (
    'county' => '台中市',
    'district' => '梧棲區',
  ),
  436 =>
  array (
    'county' => '台中市',
    'district' => '清水區',
  ),
  437 =>
  array (
    'county' => '台中市',
    'district' => '大甲區',
  ),
  438 =>
  array (
    'county' => '台中市',
    'district' => '外埔區',
  ),
  439 =>
  array (
    'county' => '台中市',
    'district' => '大安區',
  ),
  500 =>
  array (
    'county' => '彰化縣',
    'district' => '彰化市',
  ),
  502 =>
  array (
    'county' => '彰化縣',
    'district' => '芬園鄉',
  ),
  503 =>
  array (
    'county' => '彰化縣',
    'district' => '花壇鄉',
  ),
  504 =>
  array (
    'county' => '彰化縣',
    'district' => '秀水鄉',
  ),
  505 =>
  array (
    'county' => '彰化縣',
    'district' => '鹿港鎮',
  ),
  506 =>
  array (
    'county' => '彰化縣',
    'district' => '福興鄉',
  ),
  507 =>
  array (
    'county' => '彰化縣',
    'district' => '線西鄉',
  ),
  508 =>
  array (
    'county' => '彰化縣',
    'district' => '和美鎮',
  ),
  509 =>
  array (
    'county' => '彰化縣',
    'district' => '伸港鄉',
  ),
  510 =>
  array (
    'county' => '彰化縣',
    'district' => '員林鎮',
  ),
  511 =>
  array (
    'county' => '彰化縣',
    'district' => '社頭鄉',
  ),
  512 =>
  array (
    'county' => '彰化縣',
    'district' => '永靖鄉',
  ),
  513 =>
  array (
    'county' => '彰化縣',
    'district' => '埔心鄉',
  ),
  514 =>
  array (
    'county' => '彰化縣',
    'district' => '溪湖鎮',
  ),
  515 =>
  array (
    'county' => '彰化縣',
    'district' => '大村鄉',
  ),
  516 =>
  array (
    'county' => '彰化縣',
    'district' => '埔鹽鄉',
  ),
  520 =>
  array (
    'county' => '彰化縣',
    'district' => '田中鎮',
  ),
  521 =>
  array (
    'county' => '彰化縣',
    'district' => '北斗鎮',
  ),
  522 =>
  array (
    'county' => '彰化縣',
    'district' => '田尾鄉',
  ),
  523 =>
  array (
    'county' => '彰化縣',
    'district' => '埤頭鄉',
  ),
  524 =>
  array (
    'county' => '彰化縣',
    'district' => '溪州鄉',
  ),
  525 =>
  array (
    'county' => '彰化縣',
    'district' => '竹塘鄉',
  ),
  526 =>
  array (
    'county' => '彰化縣',
    'district' => '二林鎮',
  ),
  527 =>
  array (
    'county' => '彰化縣',
    'district' => '大城鄉',
  ),
  528 =>
  array (
    'county' => '彰化縣',
    'district' => '芳苑鄉',
  ),
  530 =>
  array (
    'county' => '彰化縣',
    'district' => '二水鄉',
  ),
  540 =>
  array (
    'county' => '南投縣',
    'district' => '南投市',
  ),
  541 =>
  array (
    'county' => '南投縣',
    'district' => '中寮鄉',
  ),
  542 =>
  array (
    'county' => '南投縣',
    'district' => '草屯鎮',
  ),
  544 =>
  array (
    'county' => '南投縣',
    'district' => '國姓鄉',
  ),
  545 =>
  array (
    'county' => '南投縣',
    'district' => '埔里鎮',
  ),
  546 =>
  array (
    'county' => '南投縣',
    'district' => '仁愛鄉',
  ),
  551 =>
  array (
    'county' => '南投縣',
    'district' => '名間鄉',
  ),
  552 =>
  array (
    'county' => '南投縣',
    'district' => '集集鎮',
  ),
  553 =>
  array (
    'county' => '南投縣',
    'district' => '水里鄉',
  ),
  555 =>
  array (
    'county' => '南投縣',
    'district' => '魚池鄉',
  ),
  556 =>
  array (
    'county' => '南投縣',
    'district' => '信義鄉',
  ),
  557 =>
  array (
    'county' => '南投縣',
    'district' => '竹山鎮',
  ),
  558 =>
  array (
    'county' => '南投縣',
    'district' => '鹿谷鄉',
  ),
  600 =>
  array (
    'county' => '嘉義市',
    'district' => '西區',
  ),
  602 =>
  array (
    'county' => '嘉義縣',
    'district' => '番路鄉',
  ),
  603 =>
  array (
    'county' => '嘉義縣',
    'district' => '梅山鄉',
  ),
  604 =>
  array (
    'county' => '嘉義縣',
    'district' => '竹崎鄉',
  ),
  605 =>
  array (
    'county' => '嘉義縣',
    'district' => '阿里山',
  ),
  606 =>
  array (
    'county' => '嘉義縣',
    'district' => '中埔鄉',
  ),
  607 =>
  array (
    'county' => '嘉義縣',
    'district' => '大埔鄉',
  ),
  608 =>
  array (
    'county' => '嘉義縣',
    'district' => '水上鄉',
  ),
  611 =>
  array (
    'county' => '嘉義縣',
    'district' => '鹿草鄉',
  ),
  612 =>
  array (
    'county' => '嘉義縣',
    'district' => '太保市',
  ),
  613 =>
  array (
    'county' => '嘉義縣',
    'district' => '朴子市',
  ),
  614 =>
  array (
    'county' => '嘉義縣',
    'district' => '東石鄉',
  ),
  615 =>
  array (
    'county' => '嘉義縣',
    'district' => '六腳鄉',
  ),
  616 =>
  array (
    'county' => '嘉義縣',
    'district' => '新港鄉',
  ),
  621 =>
  array (
    'county' => '嘉義縣',
    'district' => '民雄鄉',
  ),
  622 =>
  array (
    'county' => '嘉義縣',
    'district' => '大林鎮',
  ),
  623 =>
  array (
    'county' => '嘉義縣',
    'district' => '溪口鄉',
  ),
  624 =>
  array (
    'county' => '嘉義縣',
    'district' => '義竹鄉',
  ),
  625 =>
  array (
    'county' => '嘉義縣',
    'district' => '布袋鎮',
  ),
  630 =>
  array (
    'county' => '雲林縣',
    'district' => '斗南鎮',
  ),
  631 =>
  array (
    'county' => '雲林縣',
    'district' => '大埤鄉',
  ),
  632 =>
  array (
    'county' => '雲林縣',
    'district' => '虎尾鎮',
  ),
  633 =>
  array (
    'county' => '雲林縣',
    'district' => '土庫鎮',
  ),
  634 =>
  array (
    'county' => '雲林縣',
    'district' => '褒忠鄉',
  ),
  635 =>
  array (
    'county' => '雲林縣',
    'district' => '東勢鄉',
  ),
  636 =>
  array (
    'county' => '雲林縣',
    'district' => '臺西鄉',
  ),
  637 =>
  array (
    'county' => '雲林縣',
    'district' => '崙背鄉',
  ),
  638 =>
  array (
    'county' => '雲林縣',
    'district' => '麥寮鄉',
  ),
  640 =>
  array (
    'county' => '雲林縣',
    'district' => '斗六市',
  ),
  643 =>
  array (
    'county' => '雲林縣',
    'district' => '林內鄉',
  ),
  646 =>
  array (
    'county' => '雲林縣',
    'district' => '古坑鄉',
  ),
  647 =>
  array (
    'county' => '雲林縣',
    'district' => '莿桐鄉',
  ),
  648 =>
  array (
    'county' => '雲林縣',
    'district' => '西螺鎮',
  ),
  649 =>
  array (
    'county' => '雲林縣',
    'district' => '二崙鄉',
  ),
  651 =>
  array (
    'county' => '雲林縣',
    'district' => '北港鎮',
  ),
  652 =>
  array (
    'county' => '雲林縣',
    'district' => '水林鄉',
  ),
  653 =>
  array (
    'county' => '雲林縣',
    'district' => '口湖鄉',
  ),
  654 =>
  array (
    'county' => '雲林縣',
    'district' => '四湖鄉',
  ),
  655 =>
  array (
    'county' => '雲林縣',
    'district' => '元長鄉',
  ),
  700 =>
  array (
    'county' => '台南市',
    'district' => '中西區',
  ),
  701 =>
  array (
    'county' => '台南市',
    'district' => '東區',
  ),
  702 =>
  array (
    'county' => '台南市',
    'district' => '南區',
  ),
  704 =>
  array (
    'county' => '台南市',
    'district' => '北區',
  ),
  708 =>
  array (
    'county' => '台南市',
    'district' => '安平區',
  ),
  709 =>
  array (
    'county' => '台南市',
    'district' => '安南區',
  ),
  710 =>
  array (
    'county' => '台南市',
    'district' => '永康區',
  ),
  711 =>
  array (
    'county' => '台南市',
    'district' => '歸仁區',
  ),
  712 =>
  array (
    'county' => '台南市',
    'district' => '新化區',
  ),
  713 =>
  array (
    'county' => '台南市',
    'district' => '左鎮區',
  ),
  714 =>
  array (
    'county' => '台南市',
    'district' => '玉井區',
  ),
  715 =>
  array (
    'county' => '台南市',
    'district' => '楠西區',
  ),
  716 =>
  array (
    'county' => '台南市',
    'district' => '南化區',
  ),
  717 =>
  array (
    'county' => '台南市',
    'district' => '仁德區',
  ),
  718 =>
  array (
    'county' => '台南市',
    'district' => '關廟區',
  ),
  719 =>
  array (
    'county' => '台南市',
    'district' => '龍崎區',
  ),
  720 =>
  array (
    'county' => '台南市',
    'district' => '官田區',
  ),
  721 =>
  array (
    'county' => '台南市',
    'district' => '麻豆區',
  ),
  722 =>
  array (
    'county' => '台南市',
    'district' => '佳里區',
  ),
  723 =>
  array (
    'county' => '台南市',
    'district' => '西港區',
  ),
  724 =>
  array (
    'county' => '台南市',
    'district' => '七股區',
  ),
  725 =>
  array (
    'county' => '台南市',
    'district' => '將軍區',
  ),
  726 =>
  array (
    'county' => '台南市',
    'district' => '學甲區',
  ),
  727 =>
  array (
    'county' => '台南市',
    'district' => '北門區',
  ),
  730 =>
  array (
    'county' => '台南市',
    'district' => '新營區',
  ),
  731 =>
  array (
    'county' => '台南市',
    'district' => '後壁區',
  ),
  732 =>
  array (
    'county' => '台南市',
    'district' => '白河區',
  ),
  733 =>
  array (
    'county' => '台南市',
    'district' => '東山區',
  ),
  734 =>
  array (
    'county' => '台南市',
    'district' => '六甲區',
  ),
  735 =>
  array (
    'county' => '台南市',
    'district' => '下營區',
  ),
  736 =>
  array (
    'county' => '台南市',
    'district' => '柳營區',
  ),
  737 =>
  array (
    'county' => '台南市',
    'district' => '鹽水區',
  ),
  741 =>
  array (
    'county' => '台南市',
    'district' => '善化區',
  ),
  742 =>
  array (
    'county' => '台南市',
    'district' => '大內區',
  ),
  743 =>
  array (
    'county' => '台南市',
    'district' => '山上區',
  ),
  744 =>
  array (
    'county' => '台南市',
    'district' => '新市區',
  ),
  745 =>
  array (
    'county' => '台南市',
    'district' => '安定區',
  ),
  800 =>
  array (
    'county' => '高雄市',
    'district' => '新興區',
  ),
  801 =>
  array (
    'county' => '高雄市',
    'district' => '前金區',
  ),
  802 =>
  array (
    'county' => '高雄市',
    'district' => '苓雅區',
  ),
  803 =>
  array (
    'county' => '高雄市',
    'district' => '鹽埕區',
  ),
  804 =>
  array (
    'county' => '高雄市',
    'district' => '鼓山區',
  ),
  805 =>
  array (
    'county' => '高雄市',
    'district' => '旗津區',
  ),
  806 =>
  array (
    'county' => '高雄市',
    'district' => '前鎮區',
  ),
  807 =>
  array (
    'county' => '高雄市',
    'district' => '三民區',
  ),
  811 =>
  array (
    'county' => '高雄市',
    'district' => '楠梓區',
  ),
  812 =>
  array (
    'county' => '高雄市',
    'district' => '小港區',
  ),
  813 =>
  array (
    'county' => '高雄市',
    'district' => '左營區',
  ),
  814 =>
  array (
    'county' => '高雄市',
    'district' => '仁武區',
  ),
  815 =>
  array (
    'county' => '高雄市',
    'district' => '大社區',
  ),
  820 =>
  array (
    'county' => '高雄市',
    'district' => '岡山區',
  ),
  821 =>
  array (
    'county' => '高雄市',
    'district' => '路竹區',
  ),
  822 =>
  array (
    'county' => '高雄市',
    'district' => '阿蓮區',
  ),
  823 =>
  array (
    'county' => '高雄市',
    'district' => '田寮鄉',
  ),
  824 =>
  array (
    'county' => '高雄市',
    'district' => '燕巢區',
  ),
  825 =>
  array (
    'county' => '高雄市',
    'district' => '橋頭區',
  ),
  826 =>
  array (
    'county' => '高雄市',
    'district' => '梓官區',
  ),
  827 =>
  array (
    'county' => '高雄市',
    'district' => '彌陀區',
  ),
  828 =>
  array (
    'county' => '高雄市',
    'district' => '永安區',
  ),
  829 =>
  array (
    'county' => '高雄市',
    'district' => '湖內鄉',
  ),
  830 =>
  array (
    'county' => '高雄市',
    'district' => '鳳山區',
  ),
  831 =>
  array (
    'county' => '高雄市',
    'district' => '大寮區',
  ),
  832 =>
  array (
    'county' => '高雄市',
    'district' => '林園區',
  ),
  833 =>
  array (
    'county' => '高雄市',
    'district' => '鳥松區',
  ),
  840 =>
  array (
    'county' => '高雄市',
    'district' => '大樹區',
  ),
  842 =>
  array (
    'county' => '高雄市',
    'district' => '旗山區',
  ),
  843 =>
  array (
    'county' => '高雄市',
    'district' => '美濃區',
  ),
  844 =>
  array (
    'county' => '高雄市',
    'district' => '六龜區',
  ),
  845 =>
  array (
    'county' => '高雄市',
    'district' => '內門區',
  ),
  846 =>
  array (
    'county' => '高雄市',
    'district' => '杉林區',
  ),
  847 =>
  array (
    'county' => '高雄市',
    'district' => '甲仙區',
  ),
  848 =>
  array (
    'county' => '高雄市',
    'district' => '桃源區',
  ),
  849 =>
  array (
    'county' => '高雄市',
    'district' => '那瑪夏區',
  ),
  851 =>
  array (
    'county' => '高雄市',
    'district' => '茂林區',
  ),
  852 =>
  array (
    'county' => '高雄市',
    'district' => '茄萣區',
  ),
  900 =>
  array (
    'county' => '屏東縣',
    'district' => '屏東市',
  ),
  901 =>
  array (
    'county' => '屏東縣',
    'district' => '三地門',
  ),
  902 =>
  array (
    'county' => '屏東縣',
    'district' => '霧臺鄉',
  ),
  903 =>
  array (
    'county' => '屏東縣',
    'district' => '瑪家鄉',
  ),
  904 =>
  array (
    'county' => '屏東縣',
    'district' => '九如鄉',
  ),
  905 =>
  array (
    'county' => '屏東縣',
    'district' => '里港鄉',
  ),
  906 =>
  array (
    'county' => '屏東縣',
    'district' => '高樹鄉',
  ),
  907 =>
  array (
    'county' => '屏東縣',
    'district' => '鹽埔鄉',
  ),
  908 =>
  array (
    'county' => '屏東縣',
    'district' => '長治鄉',
  ),
  909 =>
  array (
    'county' => '屏東縣',
    'district' => '麟洛鄉',
  ),
  911 =>
  array (
    'county' => '屏東縣',
    'district' => '竹田鄉',
  ),
  912 =>
  array (
    'county' => '屏東縣',
    'district' => '內埔鄉',
  ),
  913 =>
  array (
    'county' => '屏東縣',
    'district' => '萬丹鄉',
  ),
  920 =>
  array (
    'county' => '屏東縣',
    'district' => '潮州鎮',
  ),
  921 =>
  array (
    'county' => '屏東縣',
    'district' => '泰武鄉',
  ),
  922 =>
  array (
    'county' => '屏東縣',
    'district' => '來義鄉',
  ),
  923 =>
  array (
    'county' => '屏東縣',
    'district' => '萬巒鄉',
  ),
  924 =>
  array (
    'county' => '屏東縣',
    'district' => '崁頂鄉',
  ),
  925 =>
  array (
    'county' => '屏東縣',
    'district' => '新埤鄉',
  ),
  926 =>
  array (
    'county' => '屏東縣',
    'district' => '南州鄉',
  ),
  927 =>
  array (
    'county' => '屏東縣',
    'district' => '林邊鄉',
  ),
  928 =>
  array (
    'county' => '屏東縣',
    'district' => '東港鎮',
  ),
  929 =>
  array (
    'county' => '屏東縣',
    'district' => '琉球鄉',
  ),
  931 =>
  array (
    'county' => '屏東縣',
    'district' => '佳冬鄉',
  ),
  932 =>
  array (
    'county' => '屏東縣',
    'district' => '新園鄉',
  ),
  940 =>
  array (
    'county' => '屏東縣',
    'district' => '枋寮鄉',
  ),
  941 =>
  array (
    'county' => '屏東縣',
    'district' => '枋山鄉',
  ),
  942 =>
  array (
    'county' => '屏東縣',
    'district' => '春日鄉',
  ),
  943 =>
  array (
    'county' => '屏東縣',
    'district' => '獅子鄉',
  ),
  944 =>
  array (
    'county' => '屏東縣',
    'district' => '車城鄉',
  ),
  945 =>
  array (
    'county' => '屏東縣',
    'district' => '牡丹鄉',
  ),
  946 =>
  array (
    'county' => '屏東縣',
    'district' => '恆春鎮',
  ),
  947 =>
  array (
    'county' => '屏東縣',
    'district' => '滿州鄉',
  ),
  950 =>
  array (
    'county' => '台東縣',
    'district' => '臺東市',
  ),
  951 =>
  array (
    'county' => '台東縣',
    'district' => '綠島鄉',
  ),
  952 =>
  array (
    'county' => '台東縣',
    'district' => '蘭嶼鄉',
  ),
  953 =>
  array (
    'county' => '台東縣',
    'district' => '延平鄉',
  ),
  954 =>
  array (
    'county' => '台東縣',
    'district' => '卑南鄉',
  ),
  955 =>
  array (
    'county' => '台東縣',
    'district' => '鹿野鄉',
  ),
  956 =>
  array (
    'county' => '台東縣',
    'district' => '關山鎮',
  ),
  957 =>
  array (
    'county' => '台東縣',
    'district' => '海端鄉',
  ),
  958 =>
  array (
    'county' => '台東縣',
    'district' => '池上鄉',
  ),
  959 =>
  array (
    'county' => '台東縣',
    'district' => '東河鄉',
  ),
  961 =>
  array (
    'county' => '台東縣',
    'district' => '成功鎮',
  ),
  962 =>
  array (
    'county' => '台東縣',
    'district' => '長濱鄉',
  ),
  963 =>
  array (
    'county' => '台東縣',
    'district' => '太麻里鄉',
  ),
  964 =>
  array (
    'county' => '台東縣',
    'district' => '金峰鄉',
  ),
  965 =>
  array (
    'county' => '台東縣',
    'district' => '大武鄉',
  ),
  966 =>
  array (
    'county' => '台東縣',
    'district' => '達仁鄉',
  ),
  970 =>
  array (
    'county' => '花蓮縣',
    'district' => '花蓮市',
  ),
  971 =>
  array (
    'county' => '花蓮縣',
    'district' => '新城鄉',
  ),
  972 =>
  array (
    'county' => '花蓮縣',
    'district' => '秀林鄉',
  ),
  973 =>
  array (
    'county' => '花蓮縣',
    'district' => '吉安鄉',
  ),
  974 =>
  array (
    'county' => '花蓮縣',
    'district' => '壽豐鄉',
  ),
  975 =>
  array (
    'county' => '花蓮縣',
    'district' => '鳳林鎮',
  ),
  976 =>
  array (
    'county' => '花蓮縣',
    'district' => '光復鄉',
  ),
  977 =>
  array (
    'county' => '花蓮縣',
    'district' => '豐濱鄉',
  ),
  978 =>
  array (
    'county' => '花蓮縣',
    'district' => '瑞穗鄉',
  ),
  979 =>
  array (
    'county' => '花蓮縣',
    'district' => '萬榮鄉',
  ),
  981 =>
  array (
    'county' => '花蓮縣',
    'district' => '玉里鎮',
  ),
  982 =>
  array (
    'county' => '花蓮縣',
    'district' => '卓溪鄉',
  ),
  983 =>
  array (
    'county' => '花蓮縣',
    'district' => '富里鄉',
  ),
  890 =>
  array (
    'county' => '金門縣',
    'district' => '金沙鎮',
  ),
  891 =>
  array (
    'county' => '金門縣',
    'district' => '金湖鎮',
  ),
  892 =>
  array (
    'county' => '金門縣',
    'district' => '金寧鄉',
  ),
  893 =>
  array (
    'county' => '金門縣',
    'district' => '金城鎮',
  ),
  894 =>
  array (
    'county' => '金門縣',
    'district' => '烈嶼鄉',
  ),
  896 =>
  array (
    'county' => '金門縣',
    'district' => '烏坵鄉',
  ),
  209 =>
  array (
    'county' => '連江縣',
    'district' => '南竿鄉',
  ),
  210 =>
  array (
    'county' => '連江縣',
    'district' => '北竿鄉',
  ),
  211 =>
  array (
    'county' => '連江縣',
    'district' => '莒光鄉',
  ),
  212 =>
  array (
    'county' => '連江縣',
    'district' => '東引鄉',
  ),
  880 =>
  array (
    'county' => '澎湖縣',
    'district' => '馬公市',
  ),
  881 =>
  array (
    'county' => '澎湖縣',
    'district' => '西嶼鄉',
  ),
  882 =>
  array (
    'county' => '澎湖縣',
    'district' => '望安鄉',
  ),
  883 =>
  array (
    'county' => '澎湖縣',
    'district' => '七美鄉',
  ),
  884 =>
  array (
    'county' => '澎湖縣',
    'district' => '白沙鄉',
  ),
  885 =>
  array (
    'county' => '澎湖縣',
    'district' => '湖西鄉',
  ),
  817 =>
  array (
    'county' => '南海諸島',
    'district' => '東沙',
  ),
  819 =>
  array (
    'county' => '南海諸島',
    'district' => '南沙',
  ),
);
