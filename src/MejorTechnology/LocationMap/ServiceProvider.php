<?php

namespace MejorTechnology\LocationMap;

use Illuminate\Support\ServiceProvider as baseServiceProvider;;


class ServiceProvider extends baseServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('locationMap', function ($app) {
            return new LocationMap();
        });
    }

    /**
     * 取得提供者所提供的服務。緩載入
     *
     * @return array
     */
    public function provides()
    {
        return ['locationMap'];
    }
}

