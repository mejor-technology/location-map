<?php

namespace MejorTechnology\LocationMap;

class LocationMap
{
    protected $zipcodeMap;
    protected $countyDistricMap;

    function __construct() {
        include __DIR__ . '/location.ini.php';
        $this->zipcodeMap = $zipcodeMapCountyAndDistrictAry;
        $this->countyDistricMap = $countyDistricMapZipcodeAry;
    }

    public function getCounty($zipcode)
    {
        return array_get($this->zipcodeMap, $zipcode . '.county');
    }

    public function getDistrict($zipcode)
    {
        return array_get($this->zipcodeMap, $zipcode . '.district');
    }
}
