<?php
namespace MejorTechnology\LocationMap\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \MejorTechnology\LocationMap
 */
class LocationMap extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'locationMap';
    }
}
